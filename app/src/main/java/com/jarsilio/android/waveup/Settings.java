/*
 * Copyright (c) 2016 Juan García Basilio
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceManager;

import com.jarsilio.android.waveup.receivers.LockScreenAdminReceiver;

import java.util.ArrayList;
import java.util.List;


public class Settings {
    public static final String ENABLED = "pref_enable";
    public static final String WAVE_MODE = "pref_wave_mode";
    public static final String POCKET_MODE = "pref_pocket_mode";
    public static final String LOCK_SCREEN = "pref_lock_screen";
    public static final String WRITE_TO_LOGCAT = "pref_write_to_logcat";
    public static final String WRITE_TO_LOGFILE = "pref_write_to_logfile";
    public static final String LOCK_SCREEN_WHEN_LANDSCAPE = "pref_lock_screen_when_landscape";
    public static final String LOCK_SCREEN_WITH_POWER_BUTTON = "pref_lock_screen_with_power_button_as_root";
    public static final String VIBRATE_ON_LOCK = "pref_lock_screen_vibrate_on_lock";


    private static volatile Settings instance;
    private final Context context;

    public static Settings getInstance(Context context) {
        if (instance == null ) {
            synchronized (Settings.class) {
                if (instance == null) {
                    instance = new Settings(context);
                }
            }
        }

        return instance;
    }

    private Settings(Context context) {
        this.context = context;
    }
    
    public SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isServiceEnabled() {
        return getPreferences().getBoolean(ENABLED, false);
    }

    public boolean isWaveMode() {
        return getPreferences().getBoolean(WAVE_MODE, false);
    }

    public boolean isPocketMode() {
        return getPreferences().getBoolean(POCKET_MODE, false);
    }

    public boolean isLockScreen() {
        return getPreferences().getBoolean(LOCK_SCREEN, false);
    }

    public boolean isLockScreenWhenLandscape() { return getPreferences().getBoolean(LOCK_SCREEN_WHEN_LANDSCAPE, false); }

    public boolean isLockScreenWithPowerButton() { return getPreferences().getBoolean(LOCK_SCREEN_WITH_POWER_BUTTON, false); }

    public boolean isVibrateWhileLocking() {
        return getPreferences().getBoolean(VIBRATE_ON_LOCK, false);
    }

    public boolean isWriteToLogcat() {
        return getPreferences().getBoolean(WRITE_TO_LOGCAT, false);
    }

    public boolean isWriteToLogfile() {
        return getPreferences().getBoolean(WRITE_TO_LOGFILE, false);
    }

    public void setPreference(CheckBoxPreference checkBox, boolean value){
        checkBox.setChecked(value);
    }

    public boolean isLockScreenAdmin() {
        ComponentName adminReceiver = new ComponentName(context, LockScreenAdminReceiver.class);
        return getPolicyManager().isAdminActive(adminReceiver);
    }

    private DevicePolicyManager getPolicyManager() {
        return (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
    }

    public List<String> getPropertyKeys() {
        List<String> propertyKeys = new ArrayList<>();
        propertyKeys.add(ENABLED);
        propertyKeys.add(WAVE_MODE);
        propertyKeys.add(POCKET_MODE);
        propertyKeys.add(LOCK_SCREEN);
        propertyKeys.add(LOCK_SCREEN_WHEN_LANDSCAPE);
        propertyKeys.add(WRITE_TO_LOGCAT);
        propertyKeys.add(WRITE_TO_LOGFILE);
        propertyKeys.add(LOCK_SCREEN_WITH_POWER_BUTTON);
        propertyKeys.add(VIBRATE_ON_LOCK);
        return propertyKeys;
    }
}
