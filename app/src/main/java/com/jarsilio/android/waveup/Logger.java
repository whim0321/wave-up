package com.jarsilio.android.waveup;

import android.content.Context;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A logger that uses the standard Android Log class to log exceptions, and also logs them to a
 * file on the device. Requires permission WRITE_EXTERNAL_STORAGE in AndroidManifest.xml.
 *
 * @author Cindy Potvin, Juan García
 */

public class Logger {
    private static final String TAG = "Logger";
    private static final int MAX_LOG_FILE_SIZE_IN_BYTES = 100000;

    public static void e(Context context, String logMessageTag, String logMessage) {
        Settings settings = Settings.getInstance(context);

        if (settings.isWriteToLogcat()) {
            Log.e(logMessageTag, logMessage);
        }
        if (settings.isWriteToLogfile()) {
            logToFile(context, logMessageTag, logMessage);
        }
    }

    public static void e(Context context, String logMessageTag, String logMessage, Throwable throwableException) {
        Settings settings = Settings.getInstance(context);

        if (settings.isWriteToLogcat()) {
            Log.e(logMessageTag, logMessage, throwableException);
        }
        if (settings.isWriteToLogfile()) {
            logToFile(context, logMessageTag, logMessage + "\r\n" + Log.getStackTraceString(throwableException));
        }
    }

    public static void i(Context context, String logMessageTag, String logMessage) {
        Settings settings = Settings.getInstance(context);

        if (settings.isWriteToLogcat()) {
            Log.i(logMessageTag, logMessage);
        }
        if (settings.isWriteToLogfile()) {
            logToFile(context, logMessageTag, logMessage);
        }
    }

    public static void i(Context context, String logMessageTag, String logMessage, Throwable throwableException) {
        Settings settings = Settings.getInstance(context);

        if (settings.isWriteToLogcat()) {
            Log.i(logMessageTag, logMessage, throwableException);
        }
        if (settings.isWriteToLogfile()) {
            logToFile(context, logMessageTag, logMessage + "\r\n" + Log.getStackTraceString(throwableException));
        }
    }

    public static void w(Context context, String logMessageTag, String logMessage) {
        Settings settings = Settings.getInstance(context);

        if (settings.isWriteToLogcat()) {
            Log.w(logMessageTag, logMessage);
        }
        if (settings.isWriteToLogfile()) {
            logToFile(context, logMessageTag, logMessage);
        }
    }

    public static void w(Context context, String logMessageTag, String logMessage, Throwable throwableException) {
        Settings settings = Settings.getInstance(context);

        if (settings.isWriteToLogcat()) {
            Log.w(logMessageTag, logMessage, throwableException);
        }
        if (settings.isWriteToLogfile()) {
            logToFile(context, logMessageTag, logMessage + "\r\n" + Log.getStackTraceString(throwableException));
        }
    }

    public static void v(Context context, String logMessageTag, String logMessage) {
        Settings settings = Settings.getInstance(context);
        
        if (settings.isWriteToLogcat()) {
            Log.v(logMessageTag, logMessage);
        }
        if (settings.isWriteToLogfile()) {
            logToFile(context, logMessageTag, logMessage);
        }
    }

    public static void v(Context context, String logMessageTag, String logMessage, Throwable throwableException) {
        Settings settings = Settings.getInstance(context);

        if (settings.isWriteToLogcat()) {
            Log.v(logMessageTag, logMessage, throwableException);
        }
        if (settings.isWriteToLogfile()) {
            logToFile(context, logMessageTag, logMessage + "\r\n" + Log.getStackTraceString(throwableException));
        }
    }

    public static void d(Context context, String logMessageTag, String logMessage) {
        Settings settings = Settings.getInstance(context);
        
        if (settings.isWriteToLogcat()) {
            Log.d(logMessageTag, logMessage);
        }
        if (settings.isWriteToLogfile()) {
            logToFile(context, logMessageTag, logMessage);
        }
    }

    public static void d(Context context, String logMessageTag, String logMessage, Throwable throwableException) {
        Settings settings = Settings.getInstance(context);

        if (settings.isWriteToLogcat()) {
            Log.d(logMessageTag, logMessage, throwableException);
        }
        if (settings.isWriteToLogfile()) {
            logToFile(context, logMessageTag, logMessage + "\r\n" + Log.getStackTraceString(throwableException));
        }
    }

    private static String getDateTimeStamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(Calendar.getInstance().getTime());
    }

    private static void logToFile(Context context, String logMessageTag, String logMessage) {
        try {
            // Gets the log file from the root of the primary storage.
            File logFile = new File(context.getExternalFilesDir(null), "WaveUp.log");
            if (!logFile.exists()) {
                logFile.createNewFile();
            } else if (logFile.length() > MAX_LOG_FILE_SIZE_IN_BYTES) {
                logFile.delete();
                logFile.createNewFile();
            }
            
            BufferedWriter writer = new BufferedWriter(new FileWriter(logFile, true));
            writer.write(String.format("%1s [%2s] %3s\r\n", getDateTimeStamp(), logMessageTag, logMessage));
            writer.close();
        } catch (IOException e) {
            Log.e(TAG, "Unable to log exception to file.", e);
        }
    }
}
